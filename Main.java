package pikachu;
import pikachu.control.GameController;

import javax.swing.*;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;

import static pikachu.utils.Utils.WINDOW_HEIGHT;
import static pikachu.utils.Utils.WINDOW_WIDTH;

public class Main{
    public static void main(String[] args) {
        GameController pikachu = new GameController("Pikachu_Group14_TKHDT2022");
        pikachu.setSize(WINDOW_WIDTH,WINDOW_HEIGHT);
        pikachu.setLocationRelativeTo(null);
        pikachu.start();
    }
}