package pikachu.control;

import pikachu.utils.Utils;
import pikachu.view.MenuView;
import pikachu.view.Pikachu;
import pikachu.view.PlayGameView;
import pikachu.view.SplashView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import static pikachu.utils.Utils.MAP_COL;
import static pikachu.utils.Utils.MAP_ROW;
import pikachu.view.PauseMenuView;
import pikachu.view.PauseMenuView.PauseMenuListener;

public class GameController extends JFrame {
	private SplashView splashView;
	private MenuView menuView;
	private PlayGameView playGameView;
	private PauseMenuView pauseMenuView;
	private Matrix matrix;
	private Timer timer;
	private int countDown;
	private int score; // luu score cong them 100 moi lan chon dung
	private int scoreSum; // luu score cua man choi truoc do, scoreHienTai = scoreSum + score;
	private int mapNumber;
	private int coupleDone;
	private ActionListener timeAction;

	public GameController(String title) throws HeadlessException {
		super(title);
		Image icon = (new ImageIcon(getClass().getResource("../resources/pika_icon.png"))).getImage();
		setIconImage(icon);
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	@Override
	protected void frameInit() {
		super.frameInit();

		// khoi tao splash view
		this.splashView = new SplashView("../resources/splash_background.png");
		this.splashView.setSize(Utils.WINDOW_WIDTH, Utils.WINDOW_HEIGHT);

		// khoi tao menu view
		this.menuView = new MenuView("../resources/menu_bg.png");
		this.menuView.setSize(Utils.WINDOW_WIDTH, Utils.WINDOW_HEIGHT);

		// khoi tao man choi
		this.playGameView = new PlayGameView(MAP_ROW, MAP_COL);
		this.playGameView.setSize(Utils.WINDOW_WIDTH, Utils.WINDOW_HEIGHT);

		// khoi tao pause menu
		this.pauseMenuView = new PauseMenuView("../resources/menu_bg.png");
		this.pauseMenuView.setSize(Utils.WINDOW_WIDTH, Utils.WINDOW_HEIGHT);

		// khoi tao ma tran thuat toan
		this.matrix = new Matrix(MAP_ROW, MAP_COL);

		this.timeAction = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				--countDown;
				playGameView.updateProgress(countDown);
				playGameView.updateTimer("Time: " + countDown);
				if (countDown == 0) {
					timer.stop();
					JOptionPane.showMessageDialog(null, "TIME OUT, GAME OVER!");
					playGameView.setVisible(false);
					menuView.setVisible(true);
				}
			}
		};

		this.timer = new Timer(1000, timeAction);

		this.splashView.setLoadingListener(new SplashView.OnLoadingListener() {

			public void onStartLoading() {

			}

			public void onLoading() {

			}

			public void onStopLoading() {
				splashView.setVisible(false);
				menuView.setVisible(true);
			}
		});

		//
		menuView.setOnClickMenuListener(new MenuView.OnClickMenuListener() {

			public void onNewGameClicked(int type) {
				menuView.setVisible(false);

				// khoi tao man choi moi
				playGameView.renderMap(matrix.renderMatrix());

				int i = (new Random()).nextInt(5);
				playGameView.setBackgroundImage("../resources/bg_" + i + ".png");

				score = 0;
				scoreSum = 0;
				mapNumber = 1;
				coupleDone = 0;

				switch (type) {
				case MenuView.TYPE_EASY:
					countDown = 180;
					break;
				case MenuView.TYPE_MEDIUM:
					countDown = 150;
					break;
				case MenuView.TYPE_HARD:
					countDown =120;
					break;
				default:
					break;
				}
				playGameView.updateMaxProgress(countDown);
				playGameView.updateScore("Score: " + score);
				playGameView.updateTimer("Time: " + countDown);
				playGameView.updateMapNum("Map: " + mapNumber);
				playGameView.setVisible(true);
				timer.start();
			}

			public void onHelpClicked() {
				Utils.debug(getClass(), "help");
			}

			public void onQuitClicked() {
				dispose();
			}

			public void onHelpClicked(int typeHelp) {

			}
		});

		this.playGameView.setPlayGameListener(new PlayGameView.PlayGameListener() {
			// Click vào game update lại thời gian và điểm số
			public void onReplayClicked() {
				playGameView.updateMap(matrix.renderMatrix());
				score = scoreSum;
				coupleDone = 0;
				countDown = playGameView.getMaxCountDown();
				playGameView.updateMaxProgress(countDown);
				playGameView.updateScore("Score: " + score);
				playGameView.updateTimer("Time: " + countDown);
				playGameView.updateMapNum("Map: " + mapNumber);
			}

			// nút tạm dừng game
			public void onPauseClicked() {
				timer.stop();
				playGameView.setVisible(false);
				pauseMenuView.setVisible(true);
			}

			// tạo border màu đỏ khi chọn đối tượng
			public void onPikachuClicked(int clickCounter, Pikachu... pikachus) {
				if (clickCounter == 1) {
					pikachus[0].drawBorder(Color.red);
				} else if (clickCounter == 2) {
					pikachus[1].drawBorder(Color.red);
					if (matrix.algorithm(pikachus[0], pikachus[1])) {
						// ẩn icon nếu chọn đúng
						matrix.setXY(pikachus[0], 0);
						matrix.setXY(pikachus[1], 0);
						// nếu 2 icon giống nhau xóa border màu đỏ
						pikachus[0].removeBorder();
						pikachus[1].removeBorder();
						// xóa 2 icon giống nhau ở vị trí chọn
						pikachus[0].setVisible(false);
						pikachus[1].setVisible(false);

						// tăng số cặp chọn đúng lên 1
						coupleDone++;

						score += 100;

						playGameView.updateScore("Score: " + score);
						// Hết đường đi game over và không chơi đủ các cặp đã cho thì game over
						if (!matrix.canPlay() && coupleDone < (matrix.getRow() - 2) * (matrix.getCol() - 2) / 2) {
							timer.stop();
							JOptionPane.showMessageDialog(null, "GAME OVER!");
							playGameView.setVisible(false);
							menuView.setVisible(true);
						}
						// Nếu chơi hết các cặp đã cho chuyển đến màn mới
						if (coupleDone == (matrix.getRow() - 2) * (matrix.getCol() - 2) / 2) {
							++mapNumber;
							if (mapNumber <= 3) {
								score = countDown * 10 + 500;
								scoreSum += score;
								score = scoreSum;
								countDown = playGameView.getMaxCountDown() - 10 * mapNumber;
								coupleDone = 0;

								playGameView.updateMaxProgress(countDown);
								playGameView.updateMap(matrix.renderMatrix());
								playGameView.updateTimer("Time: " + countDown);
								playGameView.updateMapNum("Map: " + mapNumber);
								playGameView.updateScore("Score: " + score);
							} else {
								// chơi 3 màn sẽ chiến thắng
								timer.stop();
								JOptionPane.showMessageDialog(null, "YOU WIN!");
								playGameView.setVisible(false);
								menuView.setVisible(true);
							}
						}
					} else {// nếu chọn sai xóa border đỏ đã chọn
						pikachus[0].removeBorder();
						pikachus[1].removeBorder();
						playGameView.setCountClicked(0);
					}
				}
			}
		});
		// tạo menu pause
		this.pauseMenuView.setPauseMenuListener(new PauseMenuListener() {
			// nút tiếp tục
			public void onContinueCliked() {
				pauseMenuView.setVisible(false);
				playGameView.setVisible(true);
				timer.start();
			}

			// nút quay lại menu
			public void onBackMenuClicked() {
				pauseMenuView.setVisible(false);
				menuView.setVisible(true);
			}

			// nút thoát game
			public void onQuitClicked() {
				dispose();
			}

		});

		this.add(splashView, BorderLayout.CENTER);
		this.add(menuView, BorderLayout.CENTER);
		this.add(playGameView, BorderLayout.CENTER);
		this.add(pauseMenuView, BorderLayout.CENTER);
	}

	public void start() {
		splashView.start();
		setVisible(true);
	}
}
