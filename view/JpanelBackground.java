package pikachu.view;

import pikachu.utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class JpanelBackground extends JPanel {
	protected Image backgroundImage = null;

	public JpanelBackground() {
		this(null);
	}

	// imagePath đương dẫn đến hình ảng
	// nếu đường dẫn khác null thì lấy hình ảnh đó làm background
	public JpanelBackground(String imagePath) {
		setOpaque(false);
		if (imagePath != null)
			this.backgroundImage = new ImageIcon(getClass().getResource(imagePath)).getImage();
	}

	// chỉnh kích thước phù hợp vơi khung
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (backgroundImage != null) {
			int height, width;
			height = this.getSize().height;
			width = this.getSize().width;
			g.drawImage(backgroundImage, 0, 0, width, height, this);
		}
	}

	// background
	public void setBackgroundImage(String imagePath) {
		if (imagePath != null)
			this.backgroundImage = new ImageIcon(getClass().getResource(imagePath)).getImage();
		else
			this.repaint();
	}
}
